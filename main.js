const { ipcMain } = require('electron')
const { app, BrowserWindow } = require('electron')
const { ArgumentParser } = require('argparse')
const path = require('path')
const URL = require('url')
const WebSocket  = require('ws')

const { Config, from_cli_parser } = require('./src/config.js')
const { Cache } = require('./src/cache')

const arguments = (() => {
	const parser = new ArgumentParser({
		description: 'Freechat client'
	})
	parser.add_argument('-c', '--config', {help: 'Specify config path'})
	return parser
})()


let win
let config = from_cli_parser(arguments)
let cache = new Cache()
let wsclient = null


// NOTE: this line is only here for demonstration as the current make script exposes this var
// so that we can test against self signed certificates but still use ssl
// process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = '0'

function createWin() {
	win = new BrowserWindow({
		width: 800,
		height: 600,
		minWidth: 640,
		minHeight: 480,
		webPreferences: {
			nodeIntegration: true,
			preload: path.join(__dirname + '/preload.js')
		},
		autoHideMenuBar: true,
	})

	win.loadFile('pages/index.html');

	win.on('closed', () => {
		win = null
	});
}


app.on('ready', createWin);

// on mac just because the windows are closed doesn't mean the application is too
// ! The toolbar keeps things alive against its will
app.on('window-all-closed', () => {
	if(process.platform !== 'darwin') {
		app.quit()
	}
});

app.on('activate', () => {
	if(win === null) {
		createWin();
		load_config('./example-dev-config.json'); // for now
	}
});

// returns the in-memory config reference
ipcMain.handle('config-request', async function(event, _arg) {
	return config;
})


ipcMain.handle('config-update', async function(event, updated_config) {
	config = new Config(updated_config.path, updated_config.data)
	await config.write_disk()
})


ipcMain.handle('open-socket', async function(event, kwargs ) {
	const server_conf = kwargs['conf']
	const conn_url = kwargs['url']

	const cache_target = server_conf['wsurl']

	// shutdown the websocket incase there is already one running
	if(wsclient != null) {
		console.log("Live websocket found, closing for replacement")
		wsclient.close()
		wsclient = null
	}

	wsclient = new WebSocket(conn_url)

	// Setting some listeners for the websocket
	wsclient.on('open', function() {
		cache.set_active(cache_target)
	})
	wsclient.on('close', function(code, reason) {
		console.log("Websocket close event: ",code, reason)
	})
	wsclient.on('error', function(error) {
		console.log('Websocket runtime error: ', error)
	})
	wsclient.on('message', function(message) {
		try {
			cache.new_message(cache_target, message)
		} catch (err) {
			console.log(err)
		}
	})
})

ipcMain.handle('cache-new-server', function(event, kwargs) {
	const serv_config = kwargs['cfg']
	const channels = kwargs['channels']
	const messages = kwargs['messages']
	// optional flag which tells the cache to set this as the active server
	// NOTE: Must also be the wsurl
	const active_target = kwargs['active_target'] 

	cache.add_server(active_target, serv_config, channels, messages)
	if(active_target) {
		if(active_target != serv_config['wsurl']) {
			throw 'Active target must be the same as the target\'s web-socket URL'
		}
		cache.set_active(active_target)
	}
})

ipcMain.handle('cache-append-messages', function cache_append_message(event, ...args) {
	// Used heavily by methods that call for messages en-masse
	// The whole point of this is to make it easier to dump messages into the 
	// cache without worrying so much about "how do i do that"
	const server = args[0]
	const messages = args[1]

	const wsurl = server.wsurl
	cache.append_messages(wsurl, messages)
})
