class AssertionError extends Error {
	constructor(msg) {
		super(msg)
		this.name = 'AssertionError'
	}
}


/*
 * @left left expression result to evaluate
 * @right left expression result to evaluate
 *
 * @returns void on success
 * @throws AssertionError on failure to be equal
 */
exports.eq = function(left, right) {
	if(left != right)  {
		const msg = `${left} != ${right}`
		throw new AssertionError(msg)
	}
}
