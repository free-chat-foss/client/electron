// Basically here we define various types that are used pretty much everywhere
const MESSAGE_TYPES: Array<String> = [
	'text/plain', 
	'image/png', 'image/jpeg', 'image/jpg',
	'application/webm', 'application/mp4',
	'application/mp3'
]

export class Message {
    id: BigInt  //u64
    time: BigInt//i64
    uid: BigInt //u64
    cid: BigInt //u64

    type: String
    content: String|null
    uname: String

    constructor(id: BigInt, time: BigInt, content: String, type: String, uid: BigInt, uname: String, cid: BigInt) {
        this.id = id
        this.time = time
        this.uid = uid
        this.uname = uname
        this.cid = cid

        this.type = type

		// throw away the content if its not of a valid type
        if(MESSAGE_TYPES.indexOf(type) < 0) {
            this.content = null
        } else {
            this.content = content
        }
    }
}

export class Channel {
    name: String
    type: Number
    id: BigInt
    description: String|null

    constructor(name: String, type: Number, id: BigInt, desc: String) {
        this.name = name
        this.type = type
        this.id = id
        this.description = desc
    }
}

export class ServerConfig {
    url: String
	name: String
	description: String

    constructor(json: Object) {
		this.url = json['url']
		this.name = json['name']
		this.description = json['description']
    }
}

export class UserConfig {
	id: String
	jwt: String|null
	permissions: String
	secret: String

	constructor(json: Object) {
		this.id = `${json['id']}`
		this.jwt = json['jwt']
		this.permissions = `${json['permissions']}`
		this.secret = json['secret']
	}
}
