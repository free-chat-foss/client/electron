const { ipcRenderer } = require('electron')
const { Message } = require('./types')
const URL = require('url')
const got = require('got')
const $ = require('jquery')


/**
 * @param {String} server.url 	| Hostname of target server
 *
 * @param {String} auth.jwt  | jwt for quick auth
 * @param {u64} auth.id user | id required by most/all endpoints
 *
 */
function rebind_message_box(server, auth, channel_id) {

	$('#message-btn').click( async () => {
		const content = $.trim($('#message-box').val())
		await send(server, auth, channel_id, content) 
		$('#message-box').val('')
	})

	$('#message-box').on('keypress', async (event) => {
		if(event.keyCode == 13 && !event.shiftKey ) {
			const content = $.trim($('#message-box').val())
			await send(server, auth, channel_id, content)
			$('#message-box').val('')
		}
	})
}

/**
 * @param {String} auth.jwt  | jwt for quick auth
 * @param {u64} auth.id user | id required by most/all endpoints
 *
 * @param {String} server.url 	| Url of target server
 *
 * @param {u64} channel_id   | Channel id to query
 * @param {i64} start_time   | Unix time to start fetching messages from
 * @param {i64} end_time     | Unix time to stop fetching messages from
 *
 * @returns {Message[]}
 *
 * @throws HTTPResponseError
 */
async function get_range(auth, server, channel_id, start_time, end_time, limit) {
	const url = `${server.url}/message/get_range`

	const response = await got(url, {
		searchParams: {
			id: auth.id,
			jwt: auth.jwt,
			channel_id: channel_id,
			start_time: start_time,
			end_time: end_time,
			limit: limit // backend allows for fucked up values
		},
		responseType: 'json'
	});

	// assming 200 from this point out
	let messages = []
	for(const msg of response.body['messages']) {
		console.log('raw message', msg)
		messages.push(new Message(
			msg['id'], 
			msg['time'], 
			msg['content'], 
			msg['content_type'],
			msg['author_id'],
			msg['name'],
			msg['channel_id']
		))
	}

	console.log(messages)

	rebind_message_box(server, auth, channel_id)
	return messages
}

/**
 * @param {Message} message
 *
 * @description Basically pushes the required html to the dom
 * @returns void
 */
function push_message(message) {
	let a_tag = $('<a>').attr({
		id: `message-${message.uid}`,
		href: '#',
		'class': 'btn btn-link author-name'
	}).text(message.uname)

	// next the content itself
	let content
	if(message.content) {
		content = message.content
	} else {
		content = $('<span>').text('Unsupported content')
	}

	let container = $('<p>').append(a_tag, content).attr('class', 'message')
	$('#messages-list').append( $('<li>').append(container) ) 
}

/**
 * @param {String} auth.jwt  | jwt for quick auth
 * @param {u64} auth.id user | id required by most/all endpoints
 *
 * @param {String} server.url 	| HTTP URL
 *
 * @params {u64} channel_id | channel id to query
 * @returns void
 */
exports.recent_messages = async function recent_messages(auth, server, channel_id, limit) {
	$('#messages-list').html('')

	try {
		const response = await got(server.url + '/message/recent',{
			searchParams: {
				jwt: auth.jwt,
				channel_id: channel_id,
				limit: 80
			},
			responseType: 'json'
		})
		const messages = response.body['messages']
		for(const message of messages) {
		}
	} catch (err) {
	}
	const messages = await get_range(auth, server, channel['id'], limit)

	for(const message of messages) { 
		console.log(message)
		push_message(message) 
	}
	await ipcRenderer.invoke('cache-append-messages', server, messages)
}

/**
 * @param {String} server.protocol
 * @param {String} server.hostname
 * @param {String} server.port
 *
 * @param {String} auth.jwt
 * @param {Number} auth.id
 *
 * @param {Number} channel_id
 *
 * @param {Any} body
 */
async function send(server, auth, channel_id, body) {
	const url = `${server.url}/message/send`
	// using callbacks for finer grain control of error messages
	got.post(url, {
		searchParams: { 
			id: auth.id, jwt: auth.jwt,
			channel_id: channel_id,
		},
		headers: { 'content-type': 'text/plain' },
		body: body,
	})
	.then(
		response => { 
			console.log(response);
		},
		failure => { console.log('Failed to send: ', server, auth, channel_id, failure) }
	)
	
}

/**
 * @param {String} server.protocol
 * @param {String} server.hostname
 * @param {String} server.port
 *
 * @param {String} auth.jwt
 * @param {Number} auth.id
 *
 * @param {Number} channel_id
 */
exports.fetch_from_id = async function (server, auth, channel_id) {
	// TODO: refactor to use updated methodoly
	const url = `${server.protocol}://${server.hostname}:${server.port}/message/from_id`
	const start_id = await ipcRenderer.invoke('cache-latest-msg', {
		hostname: server.hostname,
		channel: channel_id
	})

	got(url, {
		searchParams: {
			id: auth.id,
			jwt: auth.jwt,

			channel_id: channel_id,
			start: start_id + 1 // skip the last message because we already have it
		},
		responseType: 'json'
	})
	.then(
	// proper values
	messages => {
		for(const message of messages) {
			push_message(message)
		}
	},
	// Some kind of HTTP failure
	reason => {
		cosole.log('http error')
	})
	.catch(err => {
		console.error('couldn\'t handle: ', err)
	})
}
