import * as JSONBig from 'json-bigint';
import { Channel, Message } from './types';

export class Event {
	public type: string
	public message: Message|null
	// NOTE: these two fields may benefit from have a more legible property wrapping
	// them, cost would be pretty minimal(probably but idrk)
	public d_channel: Channel|null
	public c_channel: Channel|null
	


	constructor(raw: string) {
		const parsed = JSONBig.parse(raw)
		this.type = parsed['type']

		switch(this.type) {
			case 'new-message' : {
				this.message = this._message(parsed[this.type])
				this.d_channel = null
				this.c_channel = null
				break
			}
			case 'delete-channel' : {
				this.d_channel = null
				// todo
				break
			}
			case 'create-channel' : {
				this.c_channel = null
				// todo
				break
			}
			default: {
				this.message = null
				this.d_channel = null
				this.c_channel = null
			}
		}
	}

	// Attempts to setup a new Message object in the message field
	private _message(obj: Object): Message|null {
		// NOTE: this takes data from the network directly so its important
		// to keep in mind the "user-friendly" field names
		let  uid = obj['author_id']
		let  cid = obj['channel_id']
		let  content = obj['content']
		let  content_type = obj['content_type']
		let  mid = obj['id']
		let  uname = obj['name']
		let  time = obj['time']

		return new Message(mid, time, content, content_type, uid, uname, cid)
	}
}
