const { ipcRenderer } = require('electron')
const { Channel } = require('./types')
const $ = require('jquery')
const got = require('got')
const URL = require('url')

const msg = require('./messages.js')


const ANY_CHANNEL = 0
const VOICE_KIND = 1
const TEXT_KIND = 2

exports.ANY_CHANNEL = ANY_CHANNEL
exports.VOICE_KIND = VOICE_KIND
exports.TEXT_KIND = TEXT_KIND


/**
 * Channel button id's have the format of ${hostname}:${channel['name']}
 * Helper function that literally just pushes out some bs to the DOM
 *
 * @param {String} server.hostname
 * @param {u16} server.port
 * @param {String} server.protocol
 *
 * @param {u64} user.id
 * @param {String} user.jwt
 *
 * @param {u64} channel.id
 * @param {String} channel.name
 * @param {String} channel.description
 * @param {i32} channel.kind
 */
function push(server, user, channel) {
	let remote = URL.parse(server.url)
	let chan_id = `${remote.hostname}:${channel['name']}`
	$('#channels-list').append(
		$('<li>').append(
			$('<p>').append(
				$('<button>').attr({
					'class': 'btn btn-outline-secondary btn-nav-settings channel-btn',
					id: chan_id,
				}).text(channel['name'])
			)
		)
	)
	// Not using jquery for this because it breaks with weird id names really easily
	let ref = document.getElementById(chan_id)
	ref.addEventListener('click', () => {
		// now for the actual handler where we deal with clicks
		$('#channel-name').text('#' + channel['name'])
		$('#channel-description').text(channel['description'])

		const now = Math.floor(new Date().getTime()) // message times are stored in ms
		const yesterday = now - (60 * 60 * 48 * 1000) // adjust for ms
		console.log(now);

		msg.recent_messages(user, server, channel, yesterday , now, 1000)
		.then(()=>{}, reason => {
			console.error(reason)
		})
	})
}
/**
 * Listener callback for actually getting a list of channels
 * @param {Object} server Object containing server configuration
 * @param {Object} user Object containing user access data
 * @param {Number} kind 
 * 
 */
async function list(server, user, kind) {

	// TODO: caching bro
	$('#channels-list').html('')
	$('#server-name').text(server['name'])

	try {
		const url = `${server['url']}/channels/list`
		const response = await got(url, {
			searchParams: { id: user['id'], jwt: user['jwt'], kind: kind },
			responseType: 'json'
		})
		let channels_json = response.body['channels']

		let channels_parsed = []
		for(const chan of channels_json) {
			console.log("channel: ", chan)
			let c = new Channel(chan['name'], chan['kind'], chan['id'], chan['desc'])
			channels_parsed.push(c)
		}
		return channels_parsed

	} catch (err) {
		console.log(err)
		console.log(err['name'])
		return []
	}
}


/**
 * @param {String} remote.url
 * @param {String} user.id
 * @param {String} user.jwt 
 */
exports.server_button_cb = async function(remote, user) {
	let vc = await list(remote, user, VOICE_KIND) 
	let tc = await list(remote, user, TEXT_KIND)
	vc.push(...tc)

	// Assemble the DOM
	for(const chan of vc) {
		push(remote, user, chan)
	}
	// Finally update the cache
	let parts = URL.parse(remote.url)
	await ipcRenderer.invoke('cache-new-server', {
		cfg: remote,
		channels: vc, //weirdly named but it really _is_ the collection of channels
		messages: [],
		active_target: remote['wsurl']
	})
	let jwt_value = encodeURIComponent(user.jwt)
	let url = `${remote.wsurl}/text?jwt=${jwt_value}`
	await ipcRenderer.invoke('open-socket', {
		url: url,
		conf: remote
	})
}
