"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Cache = void 0;
var tslib_1 = require("tslib");
var events_1 = require("events");
var events_2 = require("./events");
var ServerCache = /** @class */ (function () {
    function ServerCache(config, channels, messages) {
        this.config = config;
        this.channels = channels;
        this.messages = new Map();
        for (var _i = 0, messages_1 = messages; _i < messages_1.length; _i++) {
            var message = messages_1[_i];
            var ref = this.messages.get(message.cid);
            if (!ref) {
                this.messages.set(message.cid, [message]);
            }
            else {
                ref.push(message);
            }
        }
        console.log('initial message cache: ', this.messages);
    }
    ServerCache.prototype.push_message = function (message) {
        // Safe way of pushing messages into the message cache
        // This will append messages one at a time
        var ref = this.messages.get(message.cid);
        if (ref) {
            ref.push(message); // NOTE: this one here?
            console.log('pushed');
        }
        else {
            this.messages.set(message.cid, [message]);
            console.log('set');
        }
        console.log('message cache state: ', this.messages.get(message.cid));
        console.log('message: ', message);
    };
    return ServerCache;
}());
var Cache = /** @class */ (function (_super) {
    tslib_1.__extends(Cache, _super);
    function Cache() {
        var _this = _super.call(this) || this;
        _this.servers = new Map();
        _this.active_host = null;
        return _this;
    }
    Cache.prototype.add_server = function (url, servercfg, channels, messages) {
        var new_entry = new ServerCache(servercfg, channels, messages);
        this.servers.set(url, new_entry);
        console.log(this.servers);
    };
    Cache.prototype.update_channels = function (hostname, channels) {
        var ref = this.servers.get(hostname);
        if (ref != undefined) {
            ref.channels = channels;
        }
    };
    Cache.prototype.last_id = function (hostname, channel_id) {
        try {
            var ref = this.servers.get(hostname).messages.get(channel_id);
            return ref[ref.length - 1].cid;
        }
        catch (_a) {
            return null;
        }
    };
    Cache.prototype.new_message = function (origin, message) {
        // we assume at this point that we have the hostname contained smoewhere
        var ref = this.servers.get(origin);
        if (ref) {
            var event_1 = new events_2.Event(message);
            ref.push_message(event_1.message);
        }
        else {
            console.error("cache::new_message No server with " + origin + " as hostname");
            console.error('cache::new_message Active host: ', this.active_host);
            console.error('cache::new_message Servers: ', this.servers);
        }
    };
    Cache.prototype.set_active = function (url) {
        this.active_host = url;
    };
    Cache.prototype.append_messages = function (origin, messages) {
        // Takes a list of messages and appends them to the relevant origin cache
        var ref = this.servers.get(origin);
        // only if that server cache is setup should we push anything to it
        if (ref) {
            for (var _i = 0, messages_2 = messages; _i < messages_2.length; _i++) {
                var msg = messages_2[_i];
                ref.push_message(msg);
            }
        }
        else {
            console.error("cache::append_messages No server with " + origin);
            console.error("cache::append_messages Active host: " + this.active_host);
            console.error('cache::append_messages Servers: ', this.servers);
        }
    };
    return Cache;
}(events_1.EventEmitter));
exports.Cache = Cache;
