import { EventEmitter } from 'events';
//import * as EventEmitter from 'events'; // unused for now but maybe useful later
import { ServerConfig, Message, Channel } from './types';
import { Event } from './events';


class ServerCache {
	// NOTE: at some point we should opt for making the messages field private and provide
	// some safer methods for data access
    channels: Array<Channel>
    // Channel-id => Array<Message>
    messages: Map<BigInt, Array<Message>>
    config: ServerConfig

    constructor(config: ServerConfig, channels: Array<Channel>, messages: Array<Message>) {
        this.config = config
        this.channels = channels
		this.messages = new Map()
        for(const message of messages) {
            const ref = this.messages.get(message.cid)

            if(!ref) {
                this.messages.set(message.cid, [message])
            } else {
                ref.push(message)
            }
        }
		console.log('initial message cache: ', this.messages)
    }

	push_message(message: Message) : void {
		// Safe way of pushing messages into the message cache
		// This will append messages one at a time
		let ref = this.messages.get(message.cid)
		if(ref) {
			ref.push(message) // NOTE: this one here?
			console.log('pushed')
		} else {
			this.messages.set(message.cid, [message])
			console.log('set')
		}
		console.log('message cache state: ', this.messages.get(message.cid))
		console.log('message: ', message)
	}
}

export class Cache extends EventEmitter {
	// hostname -> Cache
    private servers: Map<String, ServerCache>
	public active_host: String|null

    constructor() {
		super()
		this.servers = new Map()
		this.active_host = null
    }

	add_server(url: String, servercfg: ServerConfig, channels: Array<Channel>, messages: Array<Message>) : void {
		let new_entry = new ServerCache(servercfg, channels, messages)
		this.servers.set(url, new_entry)
		console.log(this.servers)
	}

	update_channels(hostname: String, channels: Array<Channel>) : void {
		let ref = this.servers.get(hostname)
		if(ref != undefined) { ref.channels = channels }
	}

	last_id(hostname: String, channel_id: BigInt) : BigInt|null {
		try {
			let ref = this.servers.get(hostname).messages.get(channel_id)
			return ref[ref.length - 1].cid
		} catch {
			return null
		}
	}

	new_message(origin: String, message: string): void {
		// we assume at this point that we have the hostname contained smoewhere
		let ref = this.servers.get(origin)
		if(ref) {
			let event = new Event(message)
			ref.push_message(event.message)
		} else {
			console.error(`cache::new_message No server with ${origin} as hostname`)
			console.error('cache::new_message Active host: ', this.active_host)
			console.error('cache::new_message Servers: ', this.servers)
		}
	}

	set_active(url: String) : void {
		this.active_host = url
	}

	append_messages(origin: string, messages: Array<Message>) : void {
		// Takes a list of messages and appends them to the relevant origin cache
		let ref = this.servers.get(origin)
		// only if that server cache is setup should we push anything to it
		if(ref) {
			for(const msg of messages) {
				ref.push_message(msg)
			}
		} else {
			console.error(`cache::append_messages No server with ${origin}`)
			console.error(`cache::append_messages Active host: ${this.active_host}`)
			console.error('cache::append_messages Servers: ', this.servers)
		}
	}
}



