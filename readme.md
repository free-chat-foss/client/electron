# Status 

This client is in desperate need of an overhaul to remove a lot of weird coding
patterns and still needs better caching support among many other things.
For the time being this client is a WIP and I don't suggest that you unironically
use it(yet).

## Client Config Description

For unix based systems configuration will be found at `$HOME/.config/freechat-client/config.json`.
There is also a commandline parameter you can pass (`-c`) which allows you to specify 
the path to a specific configuration file.
