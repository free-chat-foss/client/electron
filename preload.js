// Doing this since the projcet this base work is based off uses the same thing
// Kinda looks like something to do with compatibility so I'll keep this here
// until I know if its _actually_ required


window.addEventListener('DOMContentLoaded', () => {
	const replaceText = function (selector, text) {
		const element = document.getElementById(selector);
		if(element) { element.innerText = text; }
	}

	for(const type of ['chrome', 'node', 'electron']) {
		replaceText(`${type}`, process.versions[type]);
	}
});

setInterval(() => {
	window.setImmediate(() => {})
}, 1000)
