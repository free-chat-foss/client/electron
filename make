#!/bin/bash

# Script to help build and run this fucking thing because apparantly npm sucks for 
# projects that use cli arguments

[[ -z "$1" ]] && echo Options: run \| build && exit 0

run() {
	./node_modules/electron/dist/electron main.js $@
}

build() {
	sh scripts/build-sass.sh build
	echo tsc src/types.ts
	tsc src/types.ts
	echo tsc src/cache.ts
	tsc src/cache.ts
	echo tsc src/events.ts
	tsc src/events.ts
}

"$@"

