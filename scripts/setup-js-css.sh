#!/bin/sh

jsFolder="resources/js/"
cssFolder="resources/css/"
mkdir -p $jsFolder $cssFolder

jquery='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.slim.min.js'
bootstrapBundle='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js'

bootstrap='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'
faCss='https://use.fontawesome.com/releases/v5.8.2/css/all.css'
purecss='https://unpkg.com/purecss@1.0.1/build/pure-min.css'

for js in $jquery $bootstrapBundle;do
    wget -O $jsFolder/`basename $js` $js
done

for css in $bootstrap $faCss $purecss;do
    wget -O $cssFolder/`basename $css` $js
done